﻿using UnityEngine;

namespace Core.SelectSystem
{
    public class SelectionManager : MonoBehaviour
    {
        [SerializeField]
        private string selectableTag = "Selectable";

        private SelectableBase _selected;
        private Collider _selectedCollider;

        private Camera mainCamera;

        private void Awake()
        {
            mainCamera = Camera.main;
        }

        private void Update()
        {
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit))
            {
                if (hit.collider == _selectedCollider)
                {
                    return;
                }

                DeselectLastItem();

                if (hit.transform.CompareTag(selectableTag))
                {
                    SelectItem(hit);
                }
            }
        }

        private void SelectItem(RaycastHit hit)
        {
            _selectedCollider = hit.collider;
            _selected = hit.transform.GetComponent<SelectableBase>();
            if (_selected)
            {
                _selected.Select();
            }
        }

        private void DeselectLastItem()
        {
            _selectedCollider = null;

            if (_selected)
            {
                _selected.Deselect();
                _selected = null;
            }
        }
    }
}
