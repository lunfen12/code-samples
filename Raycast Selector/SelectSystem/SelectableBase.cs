﻿using UnityEngine;

namespace Core.SelectSystem
{
    public abstract class SelectableBase : MonoBehaviour
    {
        private void Awake()
        {
            Init();
        }

        public abstract void Init();

        public abstract void Select();

        public abstract void Deselect();
    }
}
