﻿using UnityEngine;

namespace Core.SelectSystem
{
    public class SelectableByShader : SelectableBase
    {
        [SerializeField]
        private Color selectedColor;

        private int emissionID => Shader.PropertyToID("_EmissionColor");
        private string emissionKeyword = "_EMISSION";

        private Renderer _renderer;
        public override void Init()
        {
            _renderer = GetComponent<Renderer>();
        }

        public override void Select()
        {
            _renderer.material.EnableKeyword(emissionKeyword);
            _renderer.material.SetColor(emissionID, selectedColor);
        }

        public override void Deselect()
        {
            _renderer.material.DisableKeyword(emissionKeyword);
        }
    }
}
