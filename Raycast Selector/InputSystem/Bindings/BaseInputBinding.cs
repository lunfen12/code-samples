﻿using Core.InputSystem.Actions;

namespace Core.InputSystem.Bindings
{
    public abstract class BaseInputBinding<T> where T : BaseInputAction
    {
        public abstract void SetParameters(IParameters parameters);
        protected abstract void ApplyKeys();
    }

    public interface IParameters
    {
    }
}