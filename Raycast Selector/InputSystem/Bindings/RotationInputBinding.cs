﻿using System.Collections.Generic;
using Core.InputSystem.Actions;

namespace Core.InputSystem.Bindings
{
    public class RotationInputBinding : BaseInputBinding<BaseInputAction> 
    {
        public Dictionary<MouseState, BaseInputAction> Bindings => bindings;
        
        private Dictionary<MouseState, BaseInputAction> bindings;
        private MouseActionParameters mouseActionParameters;

        public override void SetParameters(IParameters parameters)
        {
            mouseActionParameters = parameters as MouseActionParameters;
            ApplyKeys();
        }

        protected override void ApplyKeys()
        {
            bindings = new Dictionary<MouseState, BaseInputAction>
                {
                    {MouseState.Down, new MouseDownAction()},
                    {MouseState.Pressed, new MouseAction(mouseActionParameters)},
                    {MouseState.Up, new MouseUpAction()}
                };
        }
    }
}