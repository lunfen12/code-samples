﻿using System.Collections.Generic;
using Core.InputSystem.Actions;
using UnityEngine;

namespace Core.InputSystem.Bindings
{
    public class MovementInputBinding : BaseInputBinding<BaseInputAction>
    {
        public Dictionary<KeyCode, BaseInputAction> Bindings => bindings;
        private Dictionary<KeyCode, BaseInputAction> bindings;

        private MoveActionParameters _moveActionParameters;

        public override void SetParameters(IParameters parameters)
        {
            _moveActionParameters = parameters as MoveActionParameters;
            ApplyKeys();
        }

        protected override void ApplyKeys()
        {
            DirectionContainer directionContainer = new DirectionContainer
            {
                Direction = Vector3.zero
            };
            
            bindings = new Dictionary<KeyCode, BaseInputAction>
            {
                {KeyCode.W, new MoveAction(_moveActionParameters.DirectionContainer, Vector3.forward, _moveActionParameters.Boost)},
                {KeyCode.S, new MoveAction(_moveActionParameters.DirectionContainer, Vector3.back, _moveActionParameters.Boost)},
                {KeyCode.A, new MoveAction(_moveActionParameters.DirectionContainer, Vector3.left, _moveActionParameters.Boost)},
                {KeyCode.D, new MoveAction(_moveActionParameters.DirectionContainer, Vector3.right, _moveActionParameters.Boost)},
                {KeyCode.Q, new MoveAction(_moveActionParameters.DirectionContainer, Vector3.down, _moveActionParameters.Boost)},
                {KeyCode.E, new MoveAction(_moveActionParameters.DirectionContainer, Vector3.up, _moveActionParameters.Boost)},
                {KeyCode.LeftShift, new MoveBoostAction(_moveActionParameters.DirectionContainer)}
            };
        }
    }
        
    public class MoveActionParameters : IParameters
    {
        public DirectionContainer DirectionContainer;
        public CameraState TargetCameraState;
        public float Boost;
    }
}