﻿using UnityEngine;

namespace Core.InputSystem.Bindings
{
    public class MovementInput
    {
        private MovementInputBinding _movementInputBinding;
        private MoveActionParameters _moveActionParameters;
        
        public MovementInput(MoveActionParameters moveActionParameters)
        {
            _moveActionParameters = moveActionParameters;
            _movementInputBinding = new MovementInputBinding();
            _movementInputBinding.SetParameters(moveActionParameters);
        }

        public void Update()
        {
            _moveActionParameters.DirectionContainer.Direction = Vector3.zero;
            
            foreach (var key in _movementInputBinding.Bindings.Keys)
            {
                if (Input.GetKey(key))
                {
                    _movementInputBinding.Bindings[key].Invoke();
                }
            }
            
            _moveActionParameters.TargetCameraState.Translate(_moveActionParameters.DirectionContainer.Direction);
        }
    }
    
    public class DirectionContainer
    {
        public Vector3 Direction;
    }
}