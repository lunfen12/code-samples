﻿using Core.InputSystem.Actions;
using UnityEngine;
using UnityEngine.UIElements;

namespace Core.InputSystem.Bindings
{
    public class RightMouseButtonInput
    {
        private const int RightMouseButtonIndex = (int) MouseButton.RightMouse;
        private RotationInputBinding _rotationInputBinding;
        
        public RightMouseButtonInput(MouseActionParameters mouseActionParameters)
        {
            _rotationInputBinding = new RotationInputBinding();
            _rotationInputBinding.SetParameters(mouseActionParameters);
        }

        public void Update()
        {
            var rightMouseState = GetMouseState();

            if (rightMouseState != MouseState.None)
            {
                _rotationInputBinding.Bindings[rightMouseState].Invoke();
            }
        }
        
        private MouseState GetMouseState()
        {
            if (Input.GetMouseButtonDown(RightMouseButtonIndex))
            {
                return MouseState.Down;
            }
            
            if (Input.GetMouseButton(RightMouseButtonIndex))
            {
                return MouseState.Pressed;
            }
            
            return Input.GetMouseButtonUp(RightMouseButtonIndex) ? MouseState.Up : MouseState.None;
        }
    }
    
    public enum MouseState
    {
        None,
        Down,
        Pressed,
        Up
    }
}