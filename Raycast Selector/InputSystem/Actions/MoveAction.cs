﻿using Core.InputSystem.Bindings;
using UnityEngine;

namespace Core.InputSystem.Actions
{
    public class MoveAction : BaseInputAction
    {
        private DirectionContainer directionContainer;
        private Vector3 inputDirection;
        private float boost;
        
        private const float BoostScrollMultiplier = 0.2f;
        
        public MoveAction(DirectionContainer directionContainer, Vector3 inputDirection, float boost)
        {
            this.directionContainer = directionContainer;
            this.inputDirection = inputDirection;
            this.boost = boost;
        }

        public override void Invoke()
        {
            directionContainer.Direction += inputDirection * Time.deltaTime;
            boost += Input.mouseScrollDelta.y * BoostScrollMultiplier;
            directionContainer.Direction *= Mathf.Pow(2.0f, boost);
        }
    }
}