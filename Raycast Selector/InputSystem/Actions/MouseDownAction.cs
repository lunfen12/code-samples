﻿using UnityEngine;

namespace Core.InputSystem.Actions
{
    public class MouseDownAction : BaseInputAction
    {
        public override void Invoke()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}