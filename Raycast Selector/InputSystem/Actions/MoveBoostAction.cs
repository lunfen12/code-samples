﻿using Core.InputSystem.Bindings;
using UnityEngine;

namespace Core.InputSystem.Actions
{
    public class MoveBoostAction : BaseInputAction
    {
        private DirectionContainer directionContainer; 
        private Vector3 inputDirection;
        
        private const float BoostMovementMultiplier = 10f;
        
        public MoveBoostAction(DirectionContainer directionContainer)
        {
            this.directionContainer = directionContainer;
        }

        public override void Invoke()
        {
            directionContainer.Direction *= Time.deltaTime * BoostMovementMultiplier;
        }
    }
}