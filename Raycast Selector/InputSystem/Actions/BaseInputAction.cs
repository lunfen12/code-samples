﻿namespace Core.InputSystem.Actions
{
    public abstract class BaseInputAction
    {
        public abstract void Invoke();
    }
}
