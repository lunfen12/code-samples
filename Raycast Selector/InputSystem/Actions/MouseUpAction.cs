﻿using UnityEngine;

namespace Core.InputSystem.Actions
{
    public class MouseUpAction : BaseInputAction
    {
        public override void Invoke()
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}