﻿using Core.InputSystem.Bindings;
using UnityEngine;

namespace Core.InputSystem.Actions
{
    public class MouseAction : BaseInputAction
    {
        private const string MouseXaxizName = "Mouse X";
        private const string MouseYaxizName = "Mouse Y";

        private MouseActionParameters MouseActionParameters;
        
        public MouseAction(MouseActionParameters mouseActionParameters)
        {
            MouseActionParameters = mouseActionParameters;
        }

        public override void Invoke()
        {
            var mouseMovement = new Vector2(Input.GetAxis(MouseXaxizName), Input.GetAxis(MouseYaxizName) * GetInvertedMultiplier());
            var mouseSensitivityFactor = MouseActionParameters.MouseSensitivityCurve.Evaluate(mouseMovement.magnitude);

            MouseActionParameters.TargetCameraState.rotationEuler.x += mouseMovement.y * mouseSensitivityFactor;
            MouseActionParameters.TargetCameraState.rotationEuler.y += mouseMovement.x * mouseSensitivityFactor;
        }

        private int GetInvertedMultiplier() => MouseActionParameters.InvertY ? 1 : -1;
    }

    public class MouseActionParameters : IParameters
    {
        public bool InvertY;
        public CameraState TargetCameraState;
        public AnimationCurve MouseSensitivityCurve;
    }
}