﻿using UnityEngine;

namespace Core
{
    public class CameraState
    {
        public Vector3 rotationEuler;
        public Vector3 position;

        public void SetFromTransform(Transform transform)
        {
            rotationEuler = transform.eulerAngles;
            position = transform.position;
        }

        public void Translate(Vector3 translation)
        {
            Vector3 rotatedTranslation = Quaternion.Euler(rotationEuler) * translation;
            position += rotatedTranslation;
        }

        public void LerpTowards(CameraState target, float positionLerpPct, float rotationLerpPct)
        {
            rotationEuler = Vector3.Lerp(rotationEuler, target.rotationEuler, rotationLerpPct);
            position = Vector3.Lerp(position, target.position, positionLerpPct);
        }

        public void UpdateTransform(Transform transform)
        {
            transform.eulerAngles = rotationEuler;
            transform.position = position;
        }
    }
}
