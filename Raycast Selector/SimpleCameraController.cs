﻿using Core.InputSystem.Actions;
using Core.InputSystem.Bindings;
using UnityEngine;

namespace Core
{
    public class SimpleCameraController : MonoBehaviour
    {
        [Header("Movement Settings")]
        [Tooltip("Exponential boost factor on translation, controllable by mouse wheel.")]
        public float boost = 3.5f;

        [Tooltip("Time it takes to interpolate camera position 99% of the way to the target."), Range(0.001f, 1f)]
        public float positionLerpTime = 0.2f;

        [Header("Rotation Settings")]
        [Tooltip("X = Change in mouse position.\nY = Multiplicative factor for camera rotation.")]
        private static AnimationCurve mouseSensitivityCurve = new AnimationCurve(new Keyframe(0f, 0.5f, 0f, 5f), new Keyframe(1f, 2.5f, 0f, 0f));

        [Tooltip("Time it takes to interpolate camera rotation 99% of the way to the target."), Range(0.001f, 1f)]
        public float rotationLerpTime = 0.01f;

        [Tooltip("Whether or not to invert our Y axis for mouse input to rotation.")]
        private static bool invertY = false;
        
        private RightMouseButtonInput _rightMouseButtonInput;
        private MovementInput _movementInput;

        private static CameraState _targetCameraState = new CameraState();
        private readonly CameraState _interpolatingCameraState = new CameraState();

        private void Awake()
        {
            _rightMouseButtonInput = new RightMouseButtonInput(new MouseActionParameters
            {
                InvertY = invertY,
                TargetCameraState = _targetCameraState,
                MouseSensitivityCurve = mouseSensitivityCurve
            });

            _movementInput = new MovementInput(new MoveActionParameters
            {
                DirectionContainer = new DirectionContainer{Direction = Vector3.zero},
                TargetCameraState = _targetCameraState,
                Boost = boost
            });
        }

        private void OnEnable()
        {
            _targetCameraState.SetFromTransform(transform);
            _interpolatingCameraState.SetFromTransform(transform);
        }

        private void Update()
        {
            // Exit Sample  
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
				#if UNITY_EDITOR
				UnityEditor.EditorApplication.isPlaying = false; 
				#endif
            }
            
            _rightMouseButtonInput.Update();
            _movementInput.Update();

            var positionLerpPct = 1f - Mathf.Exp((Mathf.Log(1f - 0.99f) / positionLerpTime) * Time.deltaTime);
            var rotationLerpPct = 1f - Mathf.Exp((Mathf.Log(1f - 0.99f) / rotationLerpTime) * Time.deltaTime);
            
            _interpolatingCameraState.LerpTowards(_targetCameraState, positionLerpPct, rotationLerpPct);
            _interpolatingCameraState.UpdateTransform(transform);
        }
    }
}